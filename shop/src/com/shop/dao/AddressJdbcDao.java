package com.shop.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.shop.model.Address;
import com.shop.model.User;
import com.shop.util.DBUtils;

public class AddressJdbcDao implements IAddressDao {
	
	
//	 public static void main (String[] args){
//		
//		add();
//		
//	}

	public  void add( Address address,int userId) {
		// TODO Auto-generated method stub
		
		Connection conn = null;
		PreparedStatement ps = null;
		
	
		try {
			
			String sql = "insert into t_address(name,phone,postcode,user_id) value(?,?,?,?)";
			
			conn =  DBUtils.getConnection();
			ps= conn.prepareStatement(sql);
					
			
			ps.setString(1,address.getName());
			ps.setString(2,address.getPhone());
			ps.setString(3,address.getPostcode());
			ps.setInt(4, userId);
			ps.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			
			DBUtils.close(conn);
			DBUtils.close(ps);
		}
	}
		
		
		


	@Override
	public void update(Address address) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Address address() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> list(int userId) {
		// TODO Auto-generated method stub
		
		Connection conn = null;
		PreparedStatement ps= null;
		ResultSet rs = null;
		List<Address> as = new ArrayList<Address>();
		Address a = null;
		User u = null;
	    
	    
	    try {
	    	
	    	conn = DBUtils.getConnection();
		    
		    String sql ="select                                       "
		    		+ "         t1.id as 'a_id',t1.name as 'a_name',t1.phone as'a_phone',t1.postcode as'a_postcode',t2.id as 'user_id',t2.nickname ,t2.password,t2.username,t2.type as'user_type'"
		    		+ "  from                                         "
		    		+ "       t_address t1 left join t_user t2        "
		    		+ "   on                                          "
		    		+ "      t1.user_id=t2.id                         "
		    		+ "                                               "
		    		+ "  where                                        "
		    		+ "        user_id=?                                   ";
			ps= conn.prepareStatement(sql);
			ps.setInt(1,userId);
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				
				a = new Address();
				
				a.setId(rs.getInt("a_id"));
				a.setName(rs.getString("a_name"));
				a.setPhone(rs.getString("a_phone"));
				a.setPostcode(rs.getString("a_postcode"));
				
				u = new User();
				
				u.setId(rs.getInt("user_id"));
				u.setNickname(rs.getString("nickname"));
				u.setUsername(rs.getString("username"));
			
				u.setPassword(rs.getString("password"));
				u.setType(rs.getInt("user_type"));
				
				
				a.setUser(u);
				
				as.add(a);
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			DBUtils.close(rs);
			DBUtils.close(ps);
			DBUtils.close(conn);
			
		}
	    
	    
	    return as;
	    
		
		
		
		
	}

}
