package com.shop.dao;

import com.shop.model.Pager;
import com.shop.model.User;

public interface IUserDao {
	
	public User load(int id);
	public void add(User user);
	public void update(User user);
	public void delete(int id);
	public User loadByUsername(String username);
	public Pager<User> find(String name);
	public User login(String username,String password);
	

}
