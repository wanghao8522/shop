package com.shop.dao;

import org.apache.ibatis.session.SqlSession;

import com.shop.util.MybaitsUtils;

public class BaseDao<T> {
	
	public void add(T obj){
		
		SqlSession session = null;
		
		try {
		
			
			
			session = MybaitsUtils.createSession();
			
			session.insert(obj.getClass().getName()+".add",obj);
			
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			session.rollback();
			e.printStackTrace();
		} finally{
			
			MybaitsUtils.closeSession(session);
		}
	
	}

}
