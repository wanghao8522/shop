package com.shop.dao;

import java.util.List;

import com.shop.model.Address;
import com.shop.model.ShopException;
import com.shop.model.User;

public class AddressDao extends BaseDao <Address> implements IAddressDao {
	
   private	IUserDao userDao;
	public AddressDao(){
        
		userDao = DaoFactory.getUserDao();
	}

	@Override
	public void add(Address address, int userId) {
		// TODO Auto-generated method stub
		
	   User u = userDao.load(userId);
	   
	   if(u==null) new ShopException(" the address of the user is not exist!");
	   address.setUser(u);
	   
	   super.add(address);
	   
	   
	   
		 
		
		
	}

	@Override
	public void update(Address address) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Address address() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Address> list(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
