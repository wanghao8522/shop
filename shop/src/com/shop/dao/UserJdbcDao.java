package com.shop.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.shop.model.Address;
import com.shop.model.Pager;
import com.shop.model.User;
import com.shop.util.DBUtils;

public class UserJdbcDao implements IUserDao {
	
	public User load(int id){
		
		return loadOne(id);
//		
//		return loadTwo(id);
	}

	
	private User loadTwo(int id){
		
		// use two  sql selection
		
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Address> as = new ArrayList<Address>();
		User u = null;
		Address a = null;
		
		

		try {
			
			conn = DBUtils.getConnection();
			
			String sql= "select * from t_user where id=?";
	
			ps = conn.prepareStatement(sql);
			
			ps.setInt(1, id);
			rs = ps.executeQuery();
			
			while(rs.next()){
				
				u = new User();
				
				u.setId(rs.getInt("id"));
				u.setUsername(rs.getString("username"));
				u.setPassword(rs.getString("password"));
				u.setNickname(rs.getString("nickname"));
				u.setType(rs.getInt("type"));
	           
			
			}
			
		 sql = "select * from t_address where id =?";
			
		 ps=conn.prepareStatement(sql);
		 
		 ps.setInt(1, id);
		 
		 rs = ps.executeQuery();
		 
		 while(rs.next()){
			 
			 a= new Address();
			 
			 a.setId(rs.getInt("id"));
			 a.setName(rs.getString("name"));
			 a.setPhone(rs.getString("phone"));
			 a.setPostcode(rs.getString("postcode"));
		
				as.add(a);
			 
		 }
		 
		 u.setAddresses(as);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			
			DBUtils.close(ps);
			DBUtils.close(rs);
			DBUtils.close(conn);
		}
		
		
		
		
		
		return u;
		
		
	}
	


	private User loadOne(int id) {
		// TODO Auto-generated method stub
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Address> as = new ArrayList<Address>();
		User u = null;
		Address a = null;
		
		

		try {
			conn = DBUtils.getConnection();
			String sql = "select *,t2.id as 'a_id'" +
					"from t_user t1 left join t_address t2 on(t1.id=t2.user_id) where t1.id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(u==null) {
					u = new User();
					u.setId(rs.getInt("user_id"));
					u.setNickname(rs.getString("nickname"));
					u.setPassword(rs.getString("password"));
					u.setType(rs.getInt("type"));
					u.setUsername(rs.getString("username"));
				}
				a = new Address();
				a.setId(rs.getInt("a_id"));
				a.setName(rs.getString("name"));
				a.setPhone(rs.getString("phone"));
				a.setPostcode(rs.getString("postcode"));
				as.add(a);
			}
			u.setAddresses(as);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			
			DBUtils.close(ps);
			DBUtils.close(rs);
			DBUtils.close(conn);
		}
		
		
		
		
		
		return u;
		
		
	}


	@Override
	public void add(User user) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public User loadByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Pager<User> find(String name) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public User login(String username, String password) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

}
