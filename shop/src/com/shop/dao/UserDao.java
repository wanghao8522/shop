package com.shop.dao;

import java.io.InputStream;
import java.util.*;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.shop.model.Pager;
import com.shop.model.ShopException;
import com.shop.model.SystemContext;
import com.shop.model.User;
import com.shop.util.MybaitsUtils;

public class UserDao extends BaseDao<User> implements IUserDao{

	@Override
	public User load(int id) {
		// TODO Auto-generated method stub
		
		SqlSession session =null ;
		
		User u =null;
		
	
		
		
		try{
			session =MybaitsUtils.createSession();
			
			u= (User)session.selectOne(User.class.getName()+".load",id);
			
			 
			
		}finally{
			
			MybaitsUtils.closeSession(session);
		}
		
		
		
		return u;
	}

	@Override
	public void add(User user) {
		
		User tu = this.loadByUsername(user.getUsername());
		
		if(tu !=null) throw new ShopException("the user is exist!");
		
		super.add(user);
		
	}

	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		
		SqlSession session = null;
		User u = null;
		
	
			
			try {
				session = MybaitsUtils.createSession();
				
				session.update(User.class.getName()+".update",user);
				session.commit();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				session.rollback();
				
				e.printStackTrace();
			}finally{
				
				MybaitsUtils.closeSession(session);
			}
			
		
		
	}

	@Override
	public User loadByUsername(String username) {
		// TODO Auto-generated method stub
		SqlSession session= null;
		User u = null;
		
		try{
			session = MybaitsUtils.createSession();
			u = (User)session.selectOne(User.class.getName()+".load_by_username",username);
			
		}finally{
			
			MybaitsUtils.closeSession(session);
		}
		
	  
		return u;
	}

	@Override
	public Pager<User> find(String name) {
		// TODO Auto-generated method stub
		
		int pageSize = SystemContext.getPageSize();
		int pageOffset = SystemContext.getPageOffset();
		String sort = SystemContext.getSort();
		String order = SystemContext.getOrder();
		Pager<User> pages = new Pager<User>();
		SqlSession session = null;
		
		try{
			
			session = MybaitsUtils.createSession();
			
			Map<String,Object> params = new HashMap<String,Object>();
			
			if(name!=null && !name.equals(""))
			params.put("name", "%"+name+"%");
			params.put("pageSize",pageSize);
			params.put("pageOffset", pageOffset);
			params.put("order",order);
			params.put("sort", sort);
			
			
			List<User> data = session.selectList(User.class.getName()+".find",params);
			
			pages.setDatas(data);
			pages.setPageOffset(pageOffset);
			pages.setPageSize(pageSize);
			
			int totalRecord = session.selectOne(User.class.getName()+".find_count",params);
			
			pages.setTotalRecord(totalRecord);
			
		}finally{
			
			MybaitsUtils.closeSession(session);
		}
		
		
		return pages;
	}

	@Override
	public User login(String username, String password) {
		// TODO Auto-generated method stub
		
		User u = this.loadByUsername(username);
		
		if(u==null) throw new ShopException("the username is not exist!");
		if(!password.equals(u.getPassword())) throw new ShopException("the password is incorrect!");
		
		return u;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		SqlSession session = null;
		
		try {
			session = MybaitsUtils.createSession();
			
			session.delete(User.class.getName()+".delete",id);
			
			session.commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			session.rollback();
			e.printStackTrace();
		}finally{
			
			MybaitsUtils.closeSession(session);
		}
		
		
	}

}
