package com.shop.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.shop.dao.DaoFactory;
import com.shop.dao.IUserDao;
import com.shop.model.Pager;
import com.shop.model.SystemContext;
import com.shop.model.User;

public class MybaitsJunitTest {

       private  IUserDao ud;	
         	
       @Before
       public void init(){
    	   
    	   
    	   ud = DaoFactory.getUserDao();
    	   
       }
       
       @Test
       public void testAdd(){
    	   
    	   User u = new User();
    	   u.setUsername("Wednesday");
    	   u.setPassword("123");
    	   u.setNickname("big Wednesday");
    	   u.setType(1);
    	
    	   ud.add(u);
    	   
    	 
       }
	     
       
       @Test 
       public void testUpdate(){
    	   
    	 User u =  ud.loadByUsername("aaaa");
    	   u.setPassword("888");
    	   ud.update(u);
    	   
       }
       
       @Test
       public void testDelete(){
    	   
    	   ud.delete(23);
       }
       @Test
       public void testLogin(){
    	   
    	   User u = ud.login("aaaa", "888");
       }
       
       @Test
       public void testFind(){
    	   
    	   SystemContext.setPageOffset(0);
    	   SystemContext.setPageSize(15);
    	   SystemContext.setSort("id");
    	   SystemContext.setOrder("desc");
    	   Pager <User> ps=ud.find("a");
    	   System.out.println(ps.getTotalRecord());
    	    
    	   for(User u:ps.getDatas()){
    		   
    		   System.out.println(u);
    	   }
       }

}
