package com.shop.test;

import java.util.List;

import com.shop.dao.DaoFactory;
import com.shop.dao.IAddressDao;
import com.shop.dao.IUserDao;
import com.shop.model.Address;
import com.shop.model.User;

public class TestAddress {
	
	public static IAddressDao addressDao = DaoFactory.getAddressDao();
	public static IUserDao  userDao = DaoFactory.getJdbcUserDao();
	
	
	public static void main(String[] args){
		
    	testAdd();
		
//		testList();
		
		testLoadUser();
		
	}
	
	public static void testLoadUser(){
		
		User u = userDao.load(1);
		
		System.out.println(u.getNickname());
	
		for(Address a:u.getAddresses()){
			
			System.out.println(a.getName()+","+a.getPhone());
		}
		
		
	}
	
	public static void testAdd(){
		
		Address address = new Address();
		
		address.setName("11 whanga crescent Porirua");
		
		address.setPhone("0212526928");
		address.setPostcode("6034");
		addressDao.add(address, 1);
		
		
	}
	
	public static void testList(){
		
		List<Address> list = addressDao.list(1);
		
		for(Address a:list){
			
			System.out.println(a.getName()+","+a.getUser());
		}
		
		
	}
	
	

}
