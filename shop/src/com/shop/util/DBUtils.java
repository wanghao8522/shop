package com.shop.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtils {
	
	public static Connection getConnection(){
		
		Properties prop = ProperitesUtil.getJdbcProperty();
		
		String username = prop.getProperty("username");
		String password = prop.getProperty("password");
		String url= prop.getProperty("url");
		
		Connection conn =null;
		
		try {
			conn = DriverManager.getConnection(url,username,password);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return conn;
		
		
	}
	
	public static void close(Connection conn){
		
		try {
			
			
	      	if(conn!= null){
			
		
				conn.close();
				
				
	      	}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public static void close(PreparedStatement ps){
		
		
			try {
				
				if(ps !=null)
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public static void close(ResultSet rs){
		
		try {
			
			
				if(rs!=null){
					
				
						rs.close();
						
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

}
