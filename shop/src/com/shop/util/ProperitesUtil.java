package com.shop.util;

import java.io.IOException;
import java.util.Properties;

public class ProperitesUtil {
	
	private static Properties jdbcProperty;
	
	public static Properties getJdbcProperty(){
		
		
		if(jdbcProperty == null){
			
			jdbcProperty = new Properties();
			
			try {
				jdbcProperty.load(ProperitesUtil.class.getClassLoader().getResourceAsStream("jdbc.properties"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	
		}
		return jdbcProperty;
		
		
	}
	

}
